<?php
namespace models;

/**
 * Class Village
 * @property int $id
 * @property string $name
 */
class Village
{

    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }
}