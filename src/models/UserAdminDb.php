<?php


namespace models;


class UserAdminDb
{
    /**
     * @var array
     * local DB of users having rights. Like a connector table
     * [
     *   ['userId' => <userId>, 'villageId' => <villageId>, 'right' => <rightKey>]
     * ]
     */
    protected $userRightDb = [];

    /**
     * indexed by ID
     * @var User[]
     */
    protected $users = [];

    /**
     * indexed by ID
     * @var Village[]
     */
    protected $villages;


    /**
     * @param User $user
     * @param Village $village
     * @param $rightKey
     */
    public function addRightToUser(User $user, Village $village, $rightKey)
    {
        // AKA INSERT INTO user_admin ...
        if (($key = $this->findRight($user, $village, $rightKey)) === null) {
            $this->userRightDb[] = ['userId' => $user->id, 'villageId' => $village->id, 'right' => $rightKey];
        }
    }

    /**
     * @param User $user
     * @param Village $village
     * @param $rightKey
     */
    public function removeRightFromUser(User $user, Village $village, $rightKey)
    {
        // AKA DELETE FROM user_admin WHERE user_id = $user->id AND village_id = $village->id AND rightKey = $rightKey
        if (($key = $this->findRight($user, $village, $rightKey)) !== null) {
            unset($this->userRightDb[$key]);
        }
    }

    /**
     * @param User $user
     * @param Village $village
     * @param $rightKey
     * @return int|null
     */
    public function findRight(User $user, Village $village, $rightKey)
    {
        // AKA SELECT * FROM user_admin WHERE user_id = $user->id AND village_id = $village->id AND rightKey = $rightKey
        foreach ($this->userRightDb as $key => $right) {
            if ($right == ['userId' => $user->id, 'villageId' => $village->id, 'right' => $rightKey]) {
                return $key;
            }
        }

        return null;
    }

    /**
     * @param User $user
     * @param $rightKey
     * @return bool
     */
    public function hasAccessToVillages(User $user, $rightKey): bool
    {
        $rightsForPairUserRight = $this->getRightsForUser($user->id, $rightKey);
        $villageKeys = array_keys($this->villages);
        $villagesWithRight = array_map(function ($item) {
            return $item['villageId'];
        }, $rightsForPairUserRight);
        return !array_diff($villageKeys, $villagesWithRight);
    }

    /**
     * @param int $userId
     * @param string $rightKey
     * @return array
     */
    public function getRightsForUser(int $userId, string $rightKey): array
    {
        // AKA SELECT * FROM user_admin WHERE user_id = $user->id AND rightKey = $rightKey
        return array_filter($this->userRightDb, function ($item) use ($userId, $rightKey) {
            return $item['userId'] == $userId && $item['right'] == $rightKey;
        });
    }

    /**
     * @return User[]
     */
    public function getUsers(): array
    {
        return $this->users;
    }

    /**
     * @return Village[]
     */
    public function getVillages(): array
    {
        return $this->villages;
    }

    /**
     * @param $id
     * @return Village|null
     */
    public function getVillageById(int $id)
    {
        return $this->villages[$id] ?? null;
    }


    /**
     * @param $id
     * @return User|null
     */
    public function getUserById(int $id)
    {
        return $this->users[$id] ?? null;
    }

    /**
     * @param Village $village
     */
    public function setVillage(Village $village)
    {
        $this->villages[$village->id] = $village;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->users[$user->id] = $user;
    }

}