<?php

namespace models;

/**
 * Class User
 * @property int $id
 * @property string $name
 */
class User
{
    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }
}