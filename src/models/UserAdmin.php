<?php

namespace models;

use Exception;

/**
 * Class
 * @property int $id
 * @property int $villageId
 * @property int $userId
 */
class UserAdmin
{
    public const RIGHT_ADDRESS_BOOK = 'addressbook';
    public const RIGHT_SEARCH = 'search';

    public const AVAILABLE_RIGHTS = [
        self::RIGHT_ADDRESS_BOOK, self::RIGHT_SEARCH
    ];

    protected $db;

    public function __construct(UserAdminDb $db = null)
    {
        $this->db = $db ?: new UserAdminDb();
    }

    /**
     * @param integer $userId
     * @param array $rights
     *      in form of [ <right> => [<cityId> => boolean (has right or not)]]
     *      e.g. [addressbook => [ 1 => true, 2 => false ] , search => [ 1 => false, 2 => false ] ],
     */
    public function set($userId, array $rights)
    {
        $user = $this->db->getUserById($userId);

        foreach ($rights as $rightKey => $rightData) {
            foreach ($rightData as $villageId => $rightEnabled) {
                if (!($village = $this->db->getVillageById($villageId))) {
                    throw new Exception("village with ID {$villageId} not found");
                }
                if ($rightEnabled) {
                    $this->db->addRightToUser($user, $village, $rightKey);
                } else {
                    $this->db->removeRightFromUser($user, $village, $rightKey);
                }
            }
        }
    }

    /**
     * @param integer $userId
     * @param string $rightKey
     * @return array
     */
    public function get($userId, string $rightKey): array
    {
        $rightsForThisUser = $this->db->getRightsForUser($userId, $rightKey);

        $out = [];
        foreach ($rightsForThisUser as $rightForThisUser) {
            $out[] = $this->db->getVillageById($rightForThisUser['villageId']);
        }

        return $out;
    }

    /**
     * adds a village to db and provides the rights to all the users who should have it (having all the rights in all the other villages)
     * @param Village $village
     * @throws Exception
     */
    public function addVillage(Village $village)
    {
        if (!($village instanceof Village)) {
            throw new Exception('Passed object is not instance of Village class');
        }

        if ($this->db->getVillageById($village->id)) {
            throw new Exception("Village with ID {$village->id} exists already");
        }


        // users having all rights should get this right as well
        foreach ($this->db->getUsers() as $user) {
            // user has rights to all villages
            foreach (self::AVAILABLE_RIGHTS as $right) {
                if ($this->db->hasAccessToVillages($user, $right)) {
                    // user has this right to all the villages -> provide this right also to the new village
                    $this->db->addRightToUser($user, $village, $right);
                }
            }
        }

        $this->db->setVillage($village);
    }

    /**
     * adding new user entity. Adding all the rights to all the users
     * @param User $user
     */
    public function addUser(User $user)
    {
        // add all rights to new user for all villages
        foreach ($this->db->getVillages() as $village) {
            foreach (self::AVAILABLE_RIGHTS as $right) {
                $this->db->addRightToUser($user, $village, $right);
            }
        }

        $this->db->setUser($user);
    }
}