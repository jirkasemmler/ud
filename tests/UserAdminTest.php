<?php

namespace tests;

use models\User;
use models\UserAdmin;
use models\UserAdminDb;
use models\Village;
use PHPUnit\Framework\TestCase;

class UserAdminTest extends TestCase
{

    public function testIdExists()
    {
        $dbMock = \Mockery::mock(UserAdminDb::class);
        // just to show off I know mockery :D :D
        $dbMock->shouldReceive('getVillageById')->with(1)->andReturn(true);

        $this->expectException(\Exception::class);
        $userAdmin = new UserAdmin($dbMock);
        $userAdmin->addVillage((new Village(1, 'Brno')));
    }

    public function testAddUser()
    {
        $userAdmin = new UserAdmin();
        $userAdmin->addVillage((new Village(1, 'Brno')));
        $userAdmin->addVillage((new Village(2, 'Praha')));

        $user = new User(1, 'Franta');

        $userAdmin->addUser($user);

        $villagesWhereUserHasRightSearch = $userAdmin->get(1, 'search');
        $villagesWhereUserHasRightAddressBook = $userAdmin->get(1, 'search');
        $this->assertCount(2, $villagesWhereUserHasRightSearch);
        $this->assertCount(2, $villagesWhereUserHasRightAddressBook);
    }

    public function testGetAndSet()
    {
        $userAdmin = new UserAdmin();
        $userAdmin->addVillage((new Village(1, 'Brno')));
        $userAdmin->addVillage((new Village(2, 'Praha')));

        $user = new User(1, 'Franta');

        $userAdmin->addUser($user);

        $rightsBeforeUpdate = $userAdmin->get(1, 'search');

        $userAdmin->set(1, ['search' => [1 => true, 2 => false]]);
        $userAdmin->set(1, ['addressbook' => [1 => false, 2 => true]]);
        $rightsSearch = $userAdmin->get(1, 'search');
        $rightAddress = $userAdmin->get(1, 'addressbook');

        $this->assertCount(1, $rightsSearch);
        $this->assertCount(1, $rightAddress);
        $this->assertCount(2, $rightsBeforeUpdate);
        $this->assertEquals('Brno', $rightsSearch[0]->name);
        $this->assertEquals('Praha', $rightAddress[0]->name);
    }

    public function testScenarioFromTask()
    {
        $userAdmin = new UserAdmin();
        $userAdmin->addVillage((new Village(1, 'Brno')));
        $userAdmin->addVillage((new Village(2, 'Praha')));

        $userAdmin->addUser(new User(1, 'Adam'));
        $userAdmin->addUser(new User(2, 'Bob'));
        $userAdmin->addUser(new User(3, 'Cyril'));
        $userAdmin->addUser(new User(4, 'Derek'));

        // uživatel Adam má v Praze obě práva ("Adresář" a "Vyhledávač") a v Brně ani jedno.
        $userAdmin->set(1, ['search' => [1 => false, 2 => true]]);
        $userAdmin->set(1, ['addressbook' => [1 => false, 2 => true]]);

        $this->assertCount(1, $userAdmin->get(1, 'search'));
        $this->assertCount(1, $userAdmin->get(1, 'addressbook'));


        // Uživatel Bob má v Brně pouze Adresář a v Praze pouze Vyhledávač.
        $userAdmin->set(2, ['search' => [1 => false, 2 => true]]);
        $userAdmin->set(2, ['addressbook' => [1 => true, 2 => false]]);

        $this->assertCount(1, $userAdmin->get(2, 'search'));
        $this->assertCount(1, $userAdmin->get(2, 'addressbook'));


        // Uživatel Cyril má Adresář v obou městech a Vyhledávač jenom v Brně.
        $userAdmin->set(3, ['search' => [1 => true, 2 => false]]);
        $userAdmin->set(3, ['addressbook' => [1 => true, 2 => true]]);

        $this->assertCount(1, $userAdmin->get(3, 'search'));
        $this->assertCount(2, $userAdmin->get(3, 'addressbook'));


        // Uživatel Derek není vůbec v tabulce `user_admin` a tím pádem nemá žádná práva.
        $userAdmin->set(4, ['search' => [1 => false, 2 => false]]);
        $userAdmin->set(4, ['addressbook' => [1 => false, 2 => false]]);

        $this->assertCount(0, $userAdmin->get(4, 'search'));
        $this->assertCount(0, $userAdmin->get(4, 'addressbook'));


        // Pokud nového uživatele Freda přidám do `user_admin`, má bez jakékoliv další akce (ať už na úrovní aplikace či DB/trigger) automaticky všechna práva na všechna města.
        $userAdmin->addUser(new User(5, 'Fred'));

        $this->assertCount(2, $userAdmin->get(5, 'search'));
        $this->assertCount(2, $userAdmin->get(5, 'addressbook'));

        $userAdmin->addVillage((new Village(3, 'Ostrava')));

        //Fred: získá právo Adresář i Vyhledávač pro Ostravu
        $this->assertCount(3, $userAdmin->get(5, 'addressbook'));
        $this->assertCount(3, $userAdmin->get(5, 'search'));

        // Derek: nic nezíská
        $this->assertCount(0, $userAdmin->get(4, 'search'));
        $this->assertCount(0, $userAdmin->get(4, 'addressbook'));

        // Cyril: získá právo Adresář pro Ostravu
        $this->assertCount(1, $userAdmin->get(3, 'search'));
        $cyrilGet = $userAdmin->get(3, 'addressbook');
        $this->assertCount(3, $cyrilGet);
        $this->assertEquals('Ostrava', $cyrilGet[2]->name);

        // Bob: nic nezíská
        $this->assertCount(1, $userAdmin->get(2, 'search'));
        $this->assertCount(1, $userAdmin->get(2, 'addressbook'));

        // Adam: nic nezíská
        $this->assertCount(1, $userAdmin->get(1, 'search'));
        $this->assertCount(1, $userAdmin->get(1, 'addressbook'));
    }
}
